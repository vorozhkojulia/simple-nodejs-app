const express = require("express");
const process = require('process');
const os = require("os");

const app = express();
const port = process.env.PORT || "8080";

app.get("/", (req, res) => {
    res.status(200).send("Hello from nodejs");
});
app.get("/healthz", (req, res) => {
    res.status(200).send("OK");
});
app.get("/v1/user/info", (req, res) => {
    // todo: getuser info and send back in json
    res.status(200).send("User info");
});

// todo: add CRUD operations 

app.get("/health", (req, res) => {
    // timeout in seconds before health check fail
    const timeout = req.query.timeout;
    const when = req.query.when;
    var isHealthy = true;
    if ( when == "after"){
        isHealthy = process.uptime() > timeout;
    } else if ( when == "before") {
        isHealthy = process.uptime() < timeout;
    }
    if (isHealthy) {
        res.statusMessage = "Host: "+ os.hostname();
        res.status(200).send(res.statusMessage);
    } else {
        // exit with error
        res.statusMessage = "Bad Gateway. Uptime is " + process.uptime() + " seconds. <BR>App is ready only " + when + " " + timeout + " seconds pass. <br>Host: "+ os.hostname();
        res.status(502).send(res.statusMessage);
    }
});

app.listen(port, () => {
    console.log(`Listening to requests on http://localhost:${port}`);
});

