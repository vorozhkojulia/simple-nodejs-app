describe('home page', () => {
  beforeEach(() => {
    cy.visit("https://example.cypress.io")
  })


  context('header section', () => {
    it('the h2 contains the correct text', () => {
      cy.get('.container').contains('Kitchen Sink')
      cy.get('h2').contains('Commands')
    })

    it('data check', () => {
      cy.get('[data-toggle="dropdown"]').contains('Commands')
    })
  })

  context('url section', () => {
    it.only('get contains a correct linc', () => {
      cy.get('.home-list').find('a').eq(1).click()
      cy.url().should('eq', 'https://example.cypress.io/commands/querying')
    })
  })

  })