FROM node:21-alpine3.18

# Create app directory
WORKDIR /usr/src/app

COPY package.json ./

RUN npm install

# Install app
COPY index.js ./

EXPOSE 8080
CMD [ "node", "index.js" ]